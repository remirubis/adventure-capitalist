import { actionTypes } from '../actions/data';

const initialState = [{
  unlockManager: false,
  unlockManagerExp: 10000,
  unlock: true,
  unlockPricing: 0,
  multi: 1,
  name: 'lemon',
  price: 1,
  timeFactory: 1000,
  totalCostFactoryUnit: 2.65,
  image: 'https://www.flaticon.com/svg/vstatic/svg/2773/2773901.svg?token=exp=1616487974~hmac=b07a9b1fa3a29604688e23ba40ca50fe'
}, {
  unlockManager: false,
  unlockManagerExp: 100000,
  unlock: false,
  unlockPricing: 2000,
  multi: 1,
  name: 'newspaper',
  price: 60,
  timeFactory: 5000,
  totalCostFactoryUnit: 120,
  image: 'https://www.flaticon.com/svg/vstatic/svg/595/595533.svg?token=exp=1616488002~hmac=04f883d004ff64c23fdb0cfe4937c73e'
}, {
  unlockManager: false,
  unlockManagerExp: 1000000,
  unlock: false,
  unlockPricing: 100000,
  multi: 1,
  name: 'car',
  price: 1000,
  timeFactory: 10000,
  totalCostFactoryUnit: 160000,
  image: 'https://www.flaticon.com/svg/vstatic/svg/741/741407.svg?token=exp=1616488024~hmac=db086b5c892904108f5ca8736ca22e5b'
}];

const unlockProduct = (state) => {
  const stateUpdated = [...state];
  const index = stateUpdated.findIndex((item) => !item.unlock);

  stateUpdated[index].unlock = true;

  return stateUpdated;
};

const unlockManager = (state) => {
  const stateUpdated = [...state];
  const index = stateUpdated.findIndex((item) => !item.unlock);

  stateUpdated[index].unlockManager = true;

  return stateUpdated;
};

const data = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UNLOCK_MANAGER:
      return unlockManager(state);
    case actionTypes.UNLOCK_PRODUCT:
      return unlockProduct(state);
    default:
      return state;
  }
};

export default data;
